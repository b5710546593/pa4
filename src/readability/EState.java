package readability;

/**
 * State of last character which is 'E' or 'e'.
 * @author YukiReii
 *
 */
public class EState implements State{
	
	private WordCounter wordCounter;

	/**
	 * Constructor for recieve the word counter.
	 * @param wordCounter
	 */
	public EState(WordCounter wordCounter) {
		this.wordCounter = wordCounter;
	}

	@Override
	public void handleChar(char c) {
		changeState(new EState(wordCounter));
	}

	@Override
	public void changeState(State newState) {
		wordCounter.setState(newState);
	}
	
}
