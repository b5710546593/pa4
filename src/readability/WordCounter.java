package readability;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

/**
 * Word Counter is using for :
 * Counting syllables , words , sentences.
 * Calculating Flesch Readability Index.
 * Evaluating Level of Readability from text. 
 * @author Voraton Lertrattanapaisal
 *
 */
public class WordCounter {
	
	private State state;
	int wordCount;
	int sentenceCount;
	int syllableCount;
	
	/**
	 * Constructor use for initialize state, wordCount , syllableCount, sentenceCount.
	 */
	public WordCounter (){
		state = new StartState(this);
		wordCount = 0;
		syllableCount = 0;
		sentenceCount = 0;
	}
	
	/**
	 * Setting or changingthe state machine.
	 * @param newState is the state that will be changed.
	 */
	public void setState(State newState) {
		state = newState;		
	}
	
	/**
	 * Counting the syllables from the word.
	 * Updating syllableCount of global.
	 * Updating wordCount of global.
	 * @param word is word which is being calculated the syllables.
	 * @return number of syllables from the word.
	 */
	public int countSyllables(String word){
		int tempSyllable = syllableCount;
		for (int i = 0 ;i <word.length();i++){
			char c = word.charAt(i);
			if (state instanceof ConsonantState && i == word.length()-1 && "Ee".indexOf(c)>=0) state.changeState(new EState(this));
			state.handleChar(c);
		}
		if (state instanceof EState && syllableCount-tempSyllable == 0) syllableCount++; 
		int counted = syllableCount-tempSyllable;
		if (state instanceof NoneWordState || state instanceof StartState || counted == 0) {
			syllableCount = 0;
			state = new StartState(this);
			syllableCount = tempSyllable;
			return 0; 
		}
		else {
			state = new StartState(this);
			wordCount++;
			return counted;
		}
	}
	
	/**
	 * Counting number of sentence from the line that will be input.
	 * Updating syllableCount of global.
	 * Updating wordCount of global.
	 * Updating sentenceCount of global.
	 * @param line is text which is being calculated the sentence.
	 * @return the number of sentences in the line.
	 */
	public int countSentence(String line){
		String[] sentences = line.split("[\\n.\\n!\\n;\\n?]+");
		Scanner scan;
		for (String i : sentences){
			int tempCountedWord = wordCount;
			scan = new Scanner(i);
			scan.useDelimiter("[\\s,.;\\s!?()\\n[\\n]\\s/\\s\":]+");
			while (scan.hasNext()){
				countSyllables(scan.next());
			}
			int wordInSentence = wordCount-tempCountedWord;
			if (wordInSentence>0) sentenceCount++;
		}
		return sentenceCount;
	}
	
	/**
	 * Reading the text.
	 * Updating syllableCount of global.
	 * Updating wordCount of global.
	 * Updating sentenceCount of global.
	 * @param protocool is url of text.
	 */
	public void read(URL protocool){
		Scanner scanner;
		try {
			InputStream input = protocool.openStream();
			scanner = new Scanner(input);
			String text = "";
			while (scanner.hasNextLine()){
				String line = scanner.nextLine();
				text+=line;
				if (line.equals("")) text+="\n"; 
				else text += " ";
			}
			countSentence(text);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * To get the number of sentences from that text which was read.
	 * @return number of sentences from that text which was read.
	 */
	public int getCountSentence(){
		return sentenceCount;
	}
	
	/**
	 * To get the number of words from that text which was read.
	 * @return number of words from that text which was read.
	 */
	public int getCountWord(){
		return wordCount;
	}
	
	/**
	 * To get the number of syllables from that text which was read.
	 * @return number of syllables from that text which was read.
	 */
	public int getCountSyllable(){
		return syllableCount;
	}
	
	/**
	 * Calculating Flesch Readability Index.
	 * @return Flesch Readability Index of the text which was read.
	 */
	public double getFleschIndex(){
		double syllables = getCountSyllable();
		double words = getCountWord();
		double sentences = getCountSentence();
		return 206.835-84.6*(syllables/words)-1.015*(words/sentences);
	}
	
	/**
	 * Evaluating the level of readability.
	 * @return level of readability.
	 */
	public String getReadability(){
		double score = getFleschIndex();
		if (score<0) return "Advanced degree graduate";
		else if (score<31)	return "College graduate";
		else if (score<51)	return "College student";
		else if (score<61)	return "High school student";
		else if (score<66)	return "9th grade student";
		else if (score<71)	return "8th grade student";
		else if (score<81)	return "7th grade student";
		else if (score<91)	return "6th grade student";
		else if (score<101)	return "5th grade student";
		else return "4th grade student (elementary school)";
		
	}
	
	/**
	 * Reseting the word counter.
	 */
	public void reset(){
		wordCount = 0;
		syllableCount = 0;
		sentenceCount = 0;
	}

}
