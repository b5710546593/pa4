package readability;

/**
 * State of character which isn't letter.
 * @author Voraton Lertrattanapaisal
 *
 */
public class NoneWordState implements State {

	private WordCounter wordCounter;
	
	/**
	 * Constructor for recieve the word counter.
	 * @param wordCounter
	 */
	public NoneWordState(WordCounter wordCounter) {
		this.wordCounter = wordCounter;
	}

	@Override
	public void handleChar(char c) {
		changeState(new NoneWordState(wordCounter));
	}

	@Override
	public void changeState(State newState) {
		wordCounter.setState(newState);
	}

}
