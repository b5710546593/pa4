package readability;

/**
 * State of character which is letter but isn't vowel.
 * @author Voraton Lertrattanapaisal
 *
 */
public class ConsonantState implements State {

	private WordCounter wordCounter;
	
	/**
	 * Constructor for recieve the word counter.
	 * @param wordCounter
	 */
	public ConsonantState(WordCounter wordCounter) {
		this.wordCounter = wordCounter;
	}

	@Override
	public void handleChar(char c) {
		if (isVowel(c)) changeState(new VowelState(wordCounter));
		else if (isLetter(c)) changeState(new ConsonantState(wordCounter));
		else if(isSpecial(c)) changeState(new StartState(wordCounter));
		else changeState(new NoneWordState(wordCounter));
	}

	@Override
	public void changeState(State newState) {
		if (newState instanceof VowelState) wordCounter.syllableCount++;
		wordCounter.setState(newState);
	}
	
	/**
	 * Checking if c is vowel.
	 * @param c is character that was checked.
	 * @return true if c is vowel.
	 */
	public boolean isVowel(char c){
		if ("AEIOUYaeiouy".indexOf(c) >=0) return true;
		return false;
	}

	/**
	 * Checking if c is letter.
	 * @param c is chacracter that was checked.
	 * @return true if c is letter.
	 */
	public boolean isLetter(char c){
		if (Character.isLetter(c)) return true;
		return false;
	}
	
	/**
	 * Checking if c is dat or apostrophe.
	 * @param c is character that was checked.
	 * @return true if c is apostrophe or dat.
	 */
	public boolean isSpecial(char c){
		if ("-\'".indexOf(c)>=0) return true;
		return false;
	}
	

}
