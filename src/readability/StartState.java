package readability;

/**
 * State of starting to count the syllables.
 * @author Voraton Lertrattanapaisal
 *
 */
public class StartState implements State {

	private WordCounter wordCounter;
	
	/**
	 * Constructor for recieve the word counter.
	 * @param wordCounter
	 */
	public StartState(WordCounter wordCounter){
		this.wordCounter = wordCounter;
	}
	
	@Override
	public void handleChar(char c) {
		if (isVowel(c)) changeState(new VowelState(wordCounter));
		else if (isLetter(c)) changeState(new ConsonantState(wordCounter));
		else changeState(new NoneWordState(wordCounter));
	}

	@Override
	public void changeState(State newState) {
		if (newState instanceof VowelState) wordCounter.syllableCount++;
		wordCounter.setState(newState);
	}
	
	/**
	 * Checking if c is vowel.
	 * @param c is character that was checked.
	 * @return true if c is vowel.
	 */
	public boolean isVowel(char c){
		if ("AEIOUaeiou".indexOf(c) >=0) return true;
		return false;
	}

	/**
	 * Checking if c is letter.
	 * @param c is chacracter that was checked.
	 * @return true if c is letter.
	 */
	public boolean isLetter(char c){
		if (Character.isLetter(c)) return true;
		return false;
	}

}
