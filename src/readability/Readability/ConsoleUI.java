package readability.Readability;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Command Line Interface for telling information of text.
 * @author Voraton Lertrattnapaisal
 *
 */
public class ConsoleUI {
	/**
	 * main for running in command line.
	 * @param args is url of text.
	 */
	public static void main(String[] args){
		readability.WordCounter wordCounter = new readability.WordCounter();
		try {
			URL url = new URL(args[0]);
			wordCounter.read(url);
			String location = String.format("%-50s%s","Filename:",url.getFile() );
			String syllables = String.format("%-50s%d", "Number of Syllables:",wordCounter.getCountSyllable());
			String words = String.format("%-50s%d", "Number of Words:",wordCounter.getCountWord());
			String sentences = String.format("%-50s%d", "Number of Sentences:",wordCounter.getCountSentence());
			String index = String.format("%-50s%.2f", "Flesch Index:",wordCounter.getFleschIndex());
			String readability = String.format("%-50s%s", "Readability:",wordCounter.getReadability());
			String forShow = String.format("%s\n%s\n%s\n%s\n%s\n%s", location,syllables,words,sentences,index,readability);
			System.out.println(forShow);
			System.exit(1);
		} catch (MalformedURLException e) {
			System.out.println("Invalid URL");
		}
		
	}
}
