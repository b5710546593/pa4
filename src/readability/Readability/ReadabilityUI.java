package readability.Readability;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileSystemView;

/**
 * Graphical User Interface for telling information of text.
 * @author Voraton Lertrattnapaisal
 *
 */
public class ReadabilityUI extends JFrame {
	
	private readability.WordCounter wordCounter;
	private JTextField protocol;
	private JButton browse;
	private JButton count;
	private JButton clear;
	private JScrollPane scroll;
	private JTextArea show;
	
	/**
	 * Constructor to initialize the word counter ,component of JFrame and nameing the JFrame.
	 */
	public ReadabilityUI (){
		super("Readability by Voraton Lertrattanapaisal");
		super.setSize(600, 200);
		super.setResizable(false);
		wordCounter = new readability.WordCounter();
		initComponent();
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	/**
	 * Initialize component.
	 */
	public void initComponent(){
		Container container = getContentPane();
		container.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = (GridBagConstraints.HORIZONTAL);
		c.weightx = 0.5;
		Container upper = new Container();
		upper.setLayout(new FlowLayout());
		JLabel label = new JLabel("File or URL name: ");
		protocol = new JTextField(10);
		browse = new JButton("Browse...");
		browse.addActionListener(new BrowseListener());
		count = new JButton("Count");
		count.addActionListener(new CountListener());
		clear = new JButton("Clear");
		clear.addActionListener(new ClearListener());
		upper.add(label);
		upper.add(protocol);
		upper.add(browse);
		upper.add(count);
		upper.add(clear);
		
		container.add(upper,c);

		show = new JTextArea(10,1);
		scroll = new JScrollPane(show);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 100;  
		c.weightx = 1;
		c.gridwidth = 3;
		c.gridx = 0;
		c.gridy = 1;  
		container.add(scroll,c);
		
	}
	
	/**
	 * ActionListener of browseButton.
	 * @author Voraton Lertrattanapaisal
	 *
	 */
	class BrowseListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e){
			File[] sysRoot = FileSystemView.getFileSystemView().getRoots();
			File[] files = sysRoot[0].listFiles();
			File start = files[0];
			JFileChooser fileChooser = new JFileChooser(start);
			int result = fileChooser.showOpenDialog(fileChooser);
			if (result == JFileChooser.APPROVE_OPTION) {
			    File selectedFile = fileChooser.getSelectedFile();
			    try {
					protocol.setText("file://"+selectedFile.toURI().toURL().getPath());
				} catch (MalformedURLException e1) {
					e1.printStackTrace();
				}
			}
			
		}
	}
	
	/**
	 * ActionListener for countButton.
	 * @author Voraton Lertrattanapaisal
	 *
	 */
	class CountListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e){
			show.setText("");
			try {
				wordCounter.reset();
				URL url = new URL(protocol.getText());
				wordCounter.read(url);
				String location = String.format("%-50s%s","Filename:",url.getFile() );
				String syllables = String.format("%-50s%d", "Number of Syllables:",wordCounter.getCountSyllable());
				String words = String.format("%-50s%d", "Number of Words:",wordCounter.getCountWord());
				String sentences = String.format("%-50s%d", "Number of Sentences:",wordCounter.getCountSentence());
				String index = String.format("%-50s%.2f", "Flesch Index:",wordCounter.getFleschIndex());
				String readability = String.format("%-50s%s", "Readability:",wordCounter.getReadability());
				String forShow = String.format("%s\n%s\n%s\n%s\n%s\n%s", location,syllables,words,sentences,index,readability);				show.setText(forShow);
			} catch (MalformedURLException e1) {
				show.setText("Invalid URL Form!");
			}
		}
	}
	
	/**
	 * ActionListener for clearButton.
	 * @author Voraton Lertrattanapaisal
	 *
	 */
	class ClearListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			show.setText("");
		}
	}
	
	/**
	 * For show JFrame.
	 */
	public void run(){
		setVisible(true);
	}
	
	/**
	 * Main for running JFrame.
	 * @param args is doing nothing.
	 */
	public static void main(String[] args) {
		ReadabilityUI test = new ReadabilityUI();
		test.run();
		
	}
}
