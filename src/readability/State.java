package readability;

/**
 * State is state for checking character and select next state for them.
 * @author Voraton Lertrattanapaisal
 *
 */
public interface State {
	/**
	 * handleChar use for checking condition of char that was input and changeState of it.
	 * @param c is character that was input.
	 */
	public void handleChar(char c);
	/**
	 * To set new State.
	 * @param newstate new State that will be set.
	 */
	public void changeState(State newState);
	
}
